import { useContext, useEffect } from "react";

import { AppContext } from "./context";


const useAppContext = () => {
    const [state, dispatch] = useContext(AppContext);


    function updateUser(data) {
        dispatch((draft) => {
            draft.user = data;
        })
    }

    return {
        ...state,
        updateUser
    };
};

export { useAppContext };