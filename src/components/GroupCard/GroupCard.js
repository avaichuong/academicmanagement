import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        padding: "20px"
    },
    header: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: "5px 0px"
    },
    content: {
        // display: "flex",
        // justifyContent: "center"
    },
    groupTitle: {
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: "bold",
        fontSize: "1.5rem"
    }
}));

export default function GroupCard(props) {
    const classes = useStyles();

    return (
        <div className={classes.root} >
            <div className={classes.header} >
                <div className={classes.groupTitle} >{props.icon} {props.title}</div>
                <span><i class="material-icons">
                    navigate_next
                </i></span>
            </div>
            <div className={classes.content} >{props.content}</div>
        </div>
    )
}
