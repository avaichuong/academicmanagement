import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ItemNarbar from './ItemNarbar';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        color: "white"
    },
    title: {
        fontWeight: "bold"
    },
    appBar: {
        background: "#dc00f9",
        color: "#ffffff"
    }
}));

export default function ButtonAppBar() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.appBar} >
                <Toolbar style={{ justifyContent: "space-between" }} >
                    <Button className = {classes.menuButton} ><i class="material-icons">
                        arrow_back
</i></Button>
                    <Button></Button>
                </Toolbar>
            </AppBar>
        </div>
    );
}