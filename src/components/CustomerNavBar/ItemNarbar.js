import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column"
    }
}));


export default function ItemNarbar(props) {
    const classes = useStyles();
    let history = useHistory();

    function handleClickToPage() {
        // window.location.href = "" + props.item.path;
        history.push(props.item.path);
        console.log(props.item.path);
    }

    return (
        <div className={classes.root} onClick={handleClickToPage} >
            <i class="material-icons">{props.item.icon}</i>
            <div style={{ textAlign: "center", fontSize: "0.8rem", color: "#443b3b", fontWeight: "500" }} >{props.item.name}</div>
        </div>
    )
}
