import React, { useState } from 'react';
import ItemsCarousel from 'react-items-carousel';

export default (props) => {
    const [activeItemIndex, setActiveItemIndex] = useState(0);
    return (
        <>
            <ItemsCarousel
                requestToChangeActive={setActiveItemIndex}
                activeItemIndex={activeItemIndex}
                infiniteLoop={false}
                gutter={12}
                activePosition={'center'}
                chevronWidth={60}
                disableSwipe={false}
                alwaysShowChevrons={false}
                numberOfCards={props.numberCardToSee}
                slidesToScroll={props.numberCardToSee}
                outsideChevron={true}
                showSlither={true}
                firstAndLastGutter={true}
            >
                {/* <div style={{ height: "30vh", background: 'yellow' }}>First card</div>
                <div style={{ height: "30vh", background: 'yellow' }}>Second card</div>
                <div style={{ height: "30vh", background: 'yellow' }}>Third card</div>
                <div style={{ height: "30vh", background: 'yellow' }}>Fourth card</div>
                <div style={{ height: "30vh", background: 'yellow' }}>Fifth card</div>
                <div style={{ height: "30vh", background: 'yellow' }}>Sixth card</div>
                <div style={{ height: "30vh", background: 'yellow' }}>Seventh card</div> */}
                {props.content}
            </ItemsCarousel>
        </>
    );
};