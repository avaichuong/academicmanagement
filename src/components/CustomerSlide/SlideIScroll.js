// var React = require('react'),
//     ReactIScroll = require('react-iscroll'),
//     iScroll = require('iscroll');
import React from 'react';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll';

export default function SlideIScroll(props) {
    return (
        <div>
            <ReactIScroll iScroll={iScroll}
                options={{ mouseWheel: true, scrollbars: true, scrollX: true }}>
                <div style={{ width: '200%', display: "flex" }}>
                    {/* <ul> */}
                        {/* <div class="a"><div class="c"></div></div>
                        <div class="b"><div class="c"></div></div>
                        <div class="a"><div class="c"></div></div> */}
                        {/* <div class="b"><div class="c"></div></div>
                        <div class="a"><div class="c"></div></div>
                        <div class="b"><div class="c"></div></div>
                        <div class="a"><div class="c"></div></div>
                        <div class="b"><div class="c"></div></div> */}
                    {/* </ul> */}
                    {props.content}
                </div>
            </ReactIScroll>
        </div>
    )
}
