import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {
    Chip,
    LinearProgress
} from '@material-ui/core';

const useStyles = makeStyles({
    root: {
        // minWidth: 275,
        height: "100%",
        backgroundImage: "linear-gradient(#e2703f, #eea943)",
        borderRadius: "25px",
        color: "white",
        fontWeight: "bold",
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 20,
        color: "inherit",
        fontWeight: "inherit"
    },
    pos: {
        marginBottom: 12,
    },
    chip: {
        color: "inherit",
        background: "#cc88df"
    },
    progressGroup: {
        position: "absolute",
        bottom: "20%",
        width: "85%",
        display: 'flex'
    },
    progressTitle: {
        color: "inherit",
        fontWeight: "inherit"
    },
    progressBar: {
        color: "inherit"
    },
    header: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center"
    },
    icon: {
        background: "#e58b61",
        borderRadius: "50%",
        width: "40px",
        height: "40px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }

});

export default function SimpleCard() {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent style={{ height: "80%", width: "90%", position: "relative" }} >
                <div className={classes.header} >
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        Commute In
                    </Typography>
                    <span className={classes.icon} ><i class="material-icons">
                        lock
                    </i></span>
                </div>
                <div>--</div>

                <div className={classes.progressGroup} >
                    <i class="material-icons">
                        access_time
                    </i>
                    <Typography className={classes.progressTitle} color="textPrimary">
                        5-15min
                    </Typography>
                </div>
            </CardContent>
        </Card>
    );
}