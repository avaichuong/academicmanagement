import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Avatar from './Avatar';

const useStyles = makeStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundImage: "linear-gradient(#81c6f1, #7da4f3)",
        borderRadius: "20px",
        fontWeight: 'bold',
        color: 'white',
        margin: "20px"
    },
    area: {
        display: "flex",
        textAlign: "center!important",
        fontWeight: 'inherit',
        color: 'inherit'
    },
    personal: {
        display: 'flex',
        marginLeft: "20px",
        width: "70%",
        fontWeight: 'inherit',
        color: 'inherit'
    },
    groupInfo: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        fontWeight: 'inherit',
        color: 'inherit'
    },
    add: {
        display: 'flex',
        fontWeight: 'inherit',
        color: 'inherit'
    },
    follow: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: 'inherit',
        color: 'inherit'
    },
    actionGroup: {
        width: "30%",
        height: "15vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
        fontWeight: 'inherit',
        color: 'inherit'
    },
    action: {
        fontWeight: 'inherit',
        color: 'inherit'
    },
    racking: {
        fontWeight: 'inherit',
        color: 'inherit'
    },
    overView: {
        display: "flex",
        justifyContent: 'center',
        fontWeight: 'inherit',
        color: 'inherit'
    },
    name: {
        fontSize: "1.1rem"
    }
});

function Item(props) {
    return (
        <div style={{
            display: "flex",
            justifyContent: 'center',
            alignItems: "center",
            flexDirection: 'column',
            padding: "1px 20px 1px 20px",
        }}
        >
            <span>{props.data.value}</span>
            <span>{props.data.name}</span>
        </div>
    );
}


export default function ImgMediaCard(props) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActionArea className={classes.area} >
                <div className={classes.personal} >
                    <div className={classes.avatar} >
                        <Avatar size={7} />
                    </div>
                    <div className={classes.groupInfo} >
                        <div className={classes.name} >
                            Phuc Tran
                    </div>
                        <div className={classes.add} >
                            Description
                    </div>
                    </div>
                </div>
                <div className={classes.actionGroup} >
                    <Button className={classes.action} >
                        <i class="material-icons">
                            more_horiz
                        </i>
                    </Button>
                    <div className={classes.racking} >
                        <span>1</span>
                        <Typography>Ranking</Typography>
                    </div>
                </div>
            </CardActionArea>
            {/* <div className={classes.overView} >
                {
                    props.overView.map((item, index) => {
                        return (
                            <Item data={item} />
                        );
                    })
                }
            </div> */}
        </Card>
    );
}