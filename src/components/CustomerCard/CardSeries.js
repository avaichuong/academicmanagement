import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {
    Chip,
    LinearProgress
} from '@material-ui/core';

const useStyles = makeStyles({
    root: {
        // minWidth: 275,
        height: "100%",
        backgroundImage: "linear-gradient(#a665e1, #e967ba)",
        borderRadius: "25px",
        color: "white",
        fontWeight: "bold",
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 20,
        color: "inherit",
        fontWeight: "inherit"
    },
    pos: {
        marginBottom: 12,
    },
    chip: {
        color: "inherit",
        background: "#cc88df"
    },
    progressGroup: {
        position: "absolute",
        bottom: "20%",
        width: "85%"
    },
    progressTitle: {
        color: "inherit",
        fontWeight: "inherit"
    },
    progressBar:{
        color: "inherit"
    }
});

export default function SimpleCard() {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent style={{ height: "80%", width: "90%", position: "relative" }} >
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Focus Series
                </Typography>
                <Chip className={classes.chip} label="7 sessions" />

                <div className={classes.progressGroup} >
                    <Typography className={classes.progressTitle} color="textPrimary">
                        Progress: 62%
                    </Typography>
                    <LinearProgress className={classes.progressBar} variant="determinate" value={50} />
                </div>
            </CardContent>
        </Card>
    );
}