import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Avatar from './Avatar';

const useStyles = makeStyles({
    root: {
        // maxWidth: 345,
        padding: "5%",
        borderRadius: "0px 0px 40px 40px"
    },
    name: {
        textAlign: 'center'
    },
    description: {
        textAlign: 'center'
    }
});

export default function ImgMediaCard() {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <Avatar />
                <CardContent>
                    <Typography className = {classes.name} gutterBottom variant="h5" component="h2">
                        Lizard
                    </Typography>
                    <Typography className = {classes.description}  variant="body2" color="textSecondary" component="p">
                        Lizards are a widespread 
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}