import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Avatar from './Avatar';

const useStyles = makeStyles({
    root: {
        borderRadius: "20px 20px 20px 20px",
        // maxWidth: 345,
        margin: "5px",
        height: "80%",
        // width: "200px"
    },
    content: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        padding: '0!important',
        color: "black"
    },
    name: {
        textAlign: 'center'
    },
    description: {
        textAlign: 'center'
    }
});

export default function ImgMediaCard(props) {
    const classes = useStyles();

    return (
        <Card className={classes.root} style={{ width: props.slide ? "200px" : "" }} >
            <CardActionArea style={{ height: "100%" }} >
                <CardContent className={classes.content} >
                    {props.content}
                </CardContent>
            </CardActionArea>
        </Card>
    );
}