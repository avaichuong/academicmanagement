import React from 'react';
import { Switch, Route, Redirect } from "react-router-dom";
import routes from "routes.js";
// import Navbar from 'components/CustomerNavBar/Navbar';

// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footer/Footer.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import FixedPlugin from "components/FixedPlugin/FixedPlugin.js";


import styles from "assets/jss/material-dashboard-react/layouts/adminStyle.js";

import bgImage from "assets/img/sidebar-2.jpg";
import logo from "assets/img/reactlogo.png";
import Login from 'views/Authentication/Login';
import Authentication from 'views/Authentication/Authentication';

let ps;


const switchRoutes = (
    <Switch>
        {routes.map((prop, key) => {
            if (prop.layout === "/normal") {
                return (
                    <Route
                        path={prop.path}
                        component={prop.component}
                        key={key}
                    />
                );
            }
            return null;
        })}
        <Redirect from="/" to="/login" />
    </Switch>
);


const useStyles = makeStyles(styles);

export default function Normal() {
    // styles
    const classes = useStyles();
    // ref to help us initialize PerfectScrollbar on windows devices
    const mainPanel = React.createRef();
    // states and functions
    const [image, setImage] = React.useState(bgImage);
    const [color, setColor] = React.useState("blue");
    const [fixedClasses, setFixedClasses] = React.useState("dropdown show");
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [showToolBar, setShowToolBar] = React.useState(true);

    const [url, setUrl] = React.useState("");

    const handleImageClick = image => {
        setImage(image);
    };
    const handleColorClick = color => {
        setColor(color);
    };
    const handleFixedClick = () => {
        if (fixedClasses === "dropdown") {
            setFixedClasses("dropdown show");
        } else {
            setFixedClasses("dropdown");
        }
    };
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const getRoute = () => {
        return window.location.pathname !== "/admin/maps";
    };
    const resizeFunction = () => {
        if (window.innerWidth >= 960) {
            setMobileOpen(false);
            setShowToolBar(false);
        }
        else {
            setShowToolBar(true);
        }
    };
    // initialize and destroy the PerfectScrollbar plugin
    React.useEffect(() => {

        if (window.innerWidth >= 960) {
            setMobileOpen(false);
            setShowToolBar(false);
        }
        else {
            setShowToolBar(true);
        }

        if (navigator.platform.indexOf("Win") > -1) {
            ps = new PerfectScrollbar(mainPanel.current, {
                suppressScrollX: true,
                suppressScrollY: false
            });
            document.body.style.overflow = "hidden";
        }
        setUrl(window.location.pathname);
        window.addEventListener("resize", resizeFunction);
        // Specify how to clean up after this effect:
        return function cleanup() {
            if (navigator.platform.indexOf("Win") > -1) {
                ps.destroy();
            }
            window.removeEventListener("resize", resizeFunction);
        };
    }, [mainPanel]);
    return (
        <>
            {
                url.includes("login") ? <Authentication></Authentication> :
                    <div className={classes.wrapper}>

                        <Sidebar
                            routes={routes}
                            logoText={"Creative Tim"}
                            logo={logo}
                            image={image}
                            handleDrawerToggle={handleDrawerToggle}
                            open={mobileOpen}
                            color={color}
                        // {...rest}
                        />
                        <div className={classes.mainPanel} ref={mainPanel}>
                            {console.log(showToolBar)}

                            {
                                showToolBar ?
                                    <Navbar
                                        routes={routes}
                                        handleDrawerToggle={handleDrawerToggle}
                                    // {...rest}
                                    /> : <></>
                            }

                            {getRoute() ? (
                                <>
                                    <div>{switchRoutes}</div>
                                </>
                            ) : (
                                    <div className={classes.map}>{switchRoutes}</div>
                                )}

                        </div>
                    </div>
            }
        </>
    );
}
