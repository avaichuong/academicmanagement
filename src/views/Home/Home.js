import React, { useEffect } from 'react';
import Series from './Series/Series';
import Session from "./Sessions/Sessions";
import NarbarHome from 'components/CustomerNavBar/NarbarHome';
import Sidebar from 'components/Sidebar/Sidebar';
import routes from '../../routes';
import Narbar from 'components/Navbars/AdminNavbarLinks';
import { checkAuthen } from 'common/checkAuthen';
import {useHistory} from 'react-router-dom';


export default function Home() {
    const history = useHistory();
    useEffect(() => {
        if(checkAuthen()==true){
            history.push('login');
        }
    }, []);

    return (
        <div>
            {/* <NarbarHome routes={routes} /> */}
            <Series />
            <Session />
            This is home page
        </div>
    )
}
