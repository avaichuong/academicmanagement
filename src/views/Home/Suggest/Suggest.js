import React from 'react';
import GroupCard from 'components/GroupCard/GroupCard';
import Card from 'components/CustomerCard/Card';
import { makeStyles } from '@material-ui/core/styles';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";

const useStyles = makeStyles(theme => ({
    content: {
        display: "flex",
        flexDirection: "column"
    }
}));

export default function Suggest() {
    const classes = useStyles();
    return (
        <>
            <GroupCard
                title={"Đề xuất"}
                content={
                    <GridContainer>
                        <GridItem xs={6} sm={6} md={3}>
                            <Card />
                        </GridItem>
                        <GridItem xs={6} sm={6} md={3}>
                            <Card />
                        </GridItem>
                        <GridItem xs={6} sm={6} md={3}>
                            <Card />
                        </GridItem>
                        <GridItem xs={6} sm={6} md={3}>
                            <Card />
                        </GridItem>
                    </GridContainer>
                }
            />
        </>
    )
}
