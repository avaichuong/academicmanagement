import React from 'react';
import GroupCard from 'components/GroupCard/GroupCard';
import Card from 'components/CustomerCard/Card';
import { makeStyles } from '@material-ui/core/styles';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Slide from 'components/CustomerSlide/Slide';

const useStyles = makeStyles(theme => ({
    content: {
        display: "flex",
        flexDirection: "column"
    }
}));

export default function ItemAdvertisement() {
    const classes = useStyles();
    return (
        <>
            <GroupCard
                title={"Các truyện nhìn thấy trên quảng cáo đều ở đây"}
                content={
                    <>
                        <Slide numberCardToSee = {3} />
                    </>
                }
            />
        </>
    )
}
