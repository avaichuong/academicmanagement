import React from 'react';
import GroupCard from 'components/GroupCard/GroupCard';
import Card from 'components/CustomerCard/Card';
import { makeStyles } from '@material-ui/core/styles';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Slide from 'components/CustomerSlide/Slide';
import CardSeries from 'components/CustomerCard/CardSeries';

const useStyles = makeStyles(theme => ({
    content: {
        display: "flex",
        flexDirection: "column"
    }
}));

const mockArr = [1, 2, 3, 4, 5, 6, 7];

export default function Series() {
    const classes = useStyles();
    return (
        <>
            <GroupCard
                icon={<i class="material-icons">
                    assessment
                    </i>}
                title={"Series"}
                content={
                    <>
                        <Slide
                            numberCardToSee={1}
                            content={
                                mockArr.map((item, index) => {
                                    return (
                                        <div style={{ height: "30vh" }}>
                                            <CardSeries />
                                        </div>
                                    );
                                })
                            }
                        />
                    </>
                }
            />
        </>
    )
}
