import React from 'react';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    headerTab: {

    },
    headerTabLeft: {

    },
    headerTabItem: {

    },
    headerSearch: {

    }


}));

export default function Classify() {
    const classes = useStyles();

    function handleClickToChangeTab(value) {
        console.log(value);
    }

    return (
        <div>
            <div className={classes.headerTab} >
                <div className={classes.headerTabLeft} >
                    <div className={classes.headerTabItem} onClick={() => { handleClickToChangeTab(1) }} >
                        Truyện tranh
                    </div>
                    <div className={classes.headerTabItem} onClick={() => { handleClickToChangeTab(2) }}>
                        Tiểu thuyết
                    </div>
                </div>
                <div className={classes.headerSearch}>
                    <i class="material-icons">
                        search
                    </i>
                </div>
            </div>
            <div className={classes.bodyTab}>

            </div>
        </div>
    )
}
