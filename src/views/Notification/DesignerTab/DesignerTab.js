import React from 'react';
import CardDesignerTab from 'components/CustomerCard/CardDesignerTab';

const data = [
    {
        name: "Follow",
        value: '123'
    },
    {
        name: "Bucket",
        value: '456'
    },
    {
        name: "Follower",
        value: '789'
    }
]


export default function DesignerTab() {
    return (
        <div>
            <CardDesignerTab overView={data} />
            <CardDesignerTab overView={data} />
            <CardDesignerTab overView={data} />
            <CardDesignerTab overView={data} />
            <CardDesignerTab overView={data} />
            <CardDesignerTab overView={data} />
            <CardDesignerTab overView={data} />
            <CardDesignerTab overView={data} />
            <CardDesignerTab overView={data} />
            <CardDesignerTab overView={data} />
            <CardDesignerTab overView={data} />
        </div>
    )
}
