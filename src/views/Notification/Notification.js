import React from 'react';
import GroupTab from './GroupTab';
import NarbarAbout from 'components/CustomerNavBar/NarbarAbout';

export default function Notification() {
    return (
        <div>
            <NarbarAbout />
            <GroupTab />
            This is Notification
        </div>
    )
}
