import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ItemTopAction from './ItemTopAction';

const useStyles = makeStyles(theme => ({
    root: {
        // height: "20vh",
        // width: "100%",
        // background: "white",
        // clipPath: "circle(200% at 50% 450%)",
        // marginTop: "-10vh"
        display: "flex",
        justifyContent: "space-around",
        padding: "15px"
    },
}));

// height: 20vh;
//     width: 100%;
//     background: white;
//     clip-path: circle(200% at 50% 450%);
//     margin-top: -10vh;

const Action = [
    {
        name: "Thông báo"
    },
    {
        name: "Môn học"
    },
    {
        name: "Lớp học"
    },
    {
        name: "Hạnh kiểm"
    }
]

export default function TopAction() {
    const classes = useStyles();

    return (
        <div className={classes.root} >
            {
                Action.map((item, index) => {
                    return (<ItemTopAction item={item} />);
                })
            }
        </div>
    )
}
