import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column"
    }
}));


export default function ItemNarbar(props) {
    const classes = useStyles();

    return (
        <div className={classes.root} >
            <i class="material-icons">home</i>
            <div style={{ textAlign: "center", fontSize: "0.8rem", color: "#443b3b", fontWeight: "500" }} >{props.item.name}</div>
        </div>
    )
}
