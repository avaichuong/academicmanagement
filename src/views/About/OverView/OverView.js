import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        justifyContent: 'flex-end',
        background: "#dc00f9",
        color: "white",
        fontWeight: "bold"
    },
    groupOverView: {
        display: 'flex'
    }
}));

const data = [
    {
        name: "Follow",
        value: '123'
    },
    {
        name: "Bucket",
        value: '456'
    },
    {
        name: "Follower",
        value: '789'
    }
]

function Item(props) {
    return (
        <div style={{ display: "flex", justifyContent: 'center', alignItems: "center", flexDirection: 'column', padding: "1px 20px 1px 20px" }} >
            <h3>{props.data.value}</h3>
            <span>{props.data.name}</span>
        </div>
    );
}

export default function OverView() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <div className={classes.groupOverView} >
                {
                    data.map((item, index) => {
                        return (
                            <Item data={item} />
                        );
                    })
                }
            </div>
        </div>
    )
}
