import React from 'react';
import GroupCard from 'components/GroupCard/GroupCard';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CardAbout from 'components/CustomerCard/CardAbout';
// import Slide from 'components/CustomerSlide/Slide';
import Slide from 'components/CustomerSlide/SlideIScroll';

const mockArr = ["Illutration", 'Interface', "Web Application", "Photoshop", "AI", "Visual Studio"]

export default function Buckets() {
    return (
        <div>
            <GroupCard
                icon={<i class="material-icons">
                    assessment
             </i>}
                title={"Buckets"}
                content={
                    <Slide
                        numberCardToSee={1}
                        content={
                            mockArr.map((item, index) => {
                                return (
                                    <div style={{ height: "20vh", display: "inline-block" }}>
                                        <CardAbout slide = {true} content={item} />
                                    </div>
                                );
                            })
                        }
                    />
                }
            />
        </div>
    )
}
