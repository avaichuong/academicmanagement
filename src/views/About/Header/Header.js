import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import Avatar from 'components/CustomerCard/Avatar';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        justifyContent: 'space-around',
        background: "#dc00f9",
        color: "white",
        fontWeight: "bold"
    },
    personal: {
        display: 'flex',
        justifyContent: 'center'
    },
    groupInfo: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    add: {
        display: 'flex',
    },
    follow: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}));

export default function Header(props) {
    const classes = useStyles();
    console.log(props.user);

    return (
        <div className={classes.root} >
            <div className={classes.personal} >
                <div className={classes.avatar} >
                    <Avatar img={props.user ? props.user.avatar : ""} size={7} />
                </div>
                <div className={classes.groupInfo} >
                    <div className={classes.name} >
                        {props.user ? props.user.name : ""}
                    </div>
                    <div className={classes.add} >
                        <i class="material-icons">
                            phone</i>
                        {props.user ? props.user.phone : ""}
                    </div>
                </div>
            </div>
            <div className={classes.follow} >
                <Chip size="small" label="Chỉnh sửa" />
            </div>
        </div>
    )
}
