import React from 'react';
import GroupCard from 'components/GroupCard/GroupCard';
import CardAbout from 'components/CustomerCard/CardAbout';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";

export default function Shots() {
    return (
        <div>
            <GroupCard
                icon={<i class="material-icons">
                    assessment
            </i>}
                title={"Shots"}
                content={
                    <GridContainer>
                        <GridItem xs={6} sm={6} md={3}>
                            <div style={{ height: "30vh" }}>
                                <CardAbout content={
                                    <img style={{ width: '100%' }} src="https://images.pexels.com/photos/3656756/pexels-photo-3656756.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" />
                                } />
                            </div>
                        </GridItem>
                        <GridItem xs={6} sm={6} md={3}>
                            <div style={{ height: "30vh" }}>
                                <CardAbout content={
                                    <img style={{ width: '100%' }} src="https://images.pexels.com/photos/3656756/pexels-photo-3656756.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" />
                                } />
                            </div>
                        </GridItem>
                        <GridItem xs={6} sm={6} md={3}>
                            <div style={{ height: "30vh" }}>
                                <CardAbout content={
                                    <img style={{ width: '100%' }} src="https://images.pexels.com/photos/3656756/pexels-photo-3656756.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" />
                                } />
                            </div>
                        </GridItem>
                        <GridItem xs={6} sm={6} md={3}>
                            <div style={{ height: "30vh" }}>
                                <CardAbout content={
                                    <img style={{ width: '100%' }} src="https://images.pexels.com/photos/3656756/pexels-photo-3656756.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" />
                                } />
                            </div>
                        </GridItem>
                    </GridContainer>
                }
            />
        </div>
    )
}
