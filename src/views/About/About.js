import React, { useState, useEffect } from 'react';
import Narbar from 'components/CustomerNavBar/NarbarAbout';
import Header from './Header/Header';
import OverView from './OverView/OverView';
import Buckets from './Buckets/Buckets';
import Shots from './Shots/Shots';
import Slide from 'components/CustomerSlide/SlideIScroll';
import TopAction from './TopAction/TopAction';
import { checkAuthen } from 'common/checkAuthen';
import { useHistory } from 'react-router-dom';

export default function About() {
    const [user, setUser] = useState();
    const history = useHistory();

    useEffect(() => {
        if (checkAuthen() == true) {
            history.push('login');
        }
        else {
            setUser(JSON.parse(localStorage.getItem('user')));
        }
    }, []);

    return (
        <div>
            <Narbar />
            <Header user={user} />
            <OverView />
            <TopAction />
            <Buckets />
            <Shots />
        </div>
    )
}
