import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import { register } from '../../common/APICaller';
import swal from 'sweetalert';

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: "10px",
            // width: 200,
        },
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column"
    },
    button: {
        width: "50%",
        padding: "10px",
        borderRadius: "20px",
        background: "linear-gradient(to right,#f3a43b , #fce372)",
        border: "none",
        color: "#853101"
    },
    forget: {
        color: "white"
    }
}));


export default function Login() {
    const classes = useStyles();
    const [inforRegister, setInforRegister] = useState({
        name: "",
        password: "",
        confirmPassword: ""
    });

    function handChangeTextField(e) {
        setInforRegister({
            ...inforRegister,
            [e.target.name]: e.target.value
        })
    }

    function handleClickRegister() {
        console.log(inforRegister);

        if (inforRegister.password === inforRegister.confirmPassword) {
            var data = {
                name: inforRegister.name,
                pass: inforRegister.password,
                n_pass: inforRegister.confirmPassword
            }
            register("api/register", data, function (res) {
                if (res.statusText == "OK") {
                    localStorage.setItem("token", res.data.data.token);
                }
                else {
                    // console.log(res);
                    swal("Something wrong!");
                }
            })
        }
        else {
            swal("Password và Confirm Password phải giống nhau");
        }



    }


    return (
        <>
            <div className={classes.root} noValidate autoComplete="off">
                <TextField
                    name="name"
                    onChange={handChangeTextField}
                    id="name"
                    label="Name"
                />
                <TextField
                    name="password"
                    onChange={handChangeTextField}
                    id="password"
                    type="password"
                    label="Password"
                />
                <TextField
                    name="confirmPassword"
                    onChange={handChangeTextField}
                    id="confirmPassword"
                    type="password"
                    label="Confirm Password"
                />
                <button onClick={handleClickRegister} className={classes.button} >Đăng Ký</button>
            </div>
        </>
    );
}
