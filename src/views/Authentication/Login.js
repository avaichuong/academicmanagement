import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button,Checkbox } from '@material-ui/core';
import { login } from '../../common/APICaller';
import { useAppContext } from '../../views_contextApp/useContextApp';
import { useHistory } from 'react-router-dom';
import swal from 'sweetalert';

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: "10px",
            // width: 200,
        },
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column"
    },
    button: {
        width: "50%",
        padding: "10px",
        borderRadius: "20px",
        background: "linear-gradient(to right,#f3a43b , #fce372)",
        border: "none",
        color: "#853101"
    },
    colorWhite: {
        color: "white"
    }
}));


export default function Login() {
    const classes = useStyles();
    const history = useHistory();
    const [name, setName] = useState();
    const [password, setPassword] = useState();

    const {
        updateUser
    } = useAppContext();

    function handleChangeName(e) {
        // console.log(e.target.value);
        setName(e.target.value);
    }

    function handleChangePassword(e) {
        setPassword(e.target.value);
    }

    function handleLogin() {

        console.log("handle Login");
        var bodyFormData = new FormData();
        bodyFormData.set('name', name);
        bodyFormData.set('password', password);

        var data = {
            name: name,
            pass: password
        }

        console.log(bodyFormData);

        login("api/login", data, function (res) {
            if (res.statusText == "error") {
                swal("Something went wrong!");
            }
            else {
                localStorage.setItem("token", res.data.data.token);

                updateUser(res.data.data.user);
                localStorage.setItem("user", JSON.stringify(res.data.data.user));

                history.push("/about");
            }
        })
    }

    return (
        <>
            <div className={classes.root} noValidate autoComplete="off">
                <TextField onChange={handleChangeName} id="name" label="Name" />
                <TextField onChange={handleChangePassword} id="password" label="Password" type="password" />
                <button onClick={handleLogin} className={classes.button} >Đăng Nhập</button>
                <div className={classes.colorWhite} >
                    Quên mật khẩu
                </div>
                <div className={classes.rememberLogin} >
                    <Checkbox
                        // checked={checked}
                        // onChange={handleChange}
                        value="primary"
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                    <span className = {classes.colorWhite} >Nhớ mật khẩu</span>
                </div>
            </div>
        </>
    );
}
