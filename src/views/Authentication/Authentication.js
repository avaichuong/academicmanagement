import React from 'react';
import NarbarLogin from 'components/CustomerNavBar/NarbarLogin';
import TabAuthen from './TabAuthen';
import Logo from './Logo';
import { useAppContext } from '../../views_contextApp/useContextApp';
import { makeStyles, useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        backgroundImage: `url(${"images/bg_red.jpg"})`,
        height: "100vh",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        // position: "relative",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
}));


export default function Authentication() {

    const classes = useStyles();
    const {

    } = useAppContext();

    return (
        <div className={classes.root} >
            {/* <NarbarLogin /> */}
            <div>
                <Logo />
                <TabAuthen />
            </div>
        </div>
    )
}
