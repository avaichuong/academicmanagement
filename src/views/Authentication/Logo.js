import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        // width: 500,
    },
    logo: {
        height: "80px",
        width: "80px"
    },
    logoGroup:{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        padding: "20px"
    }
}));


export default function Logo() {

    const classes = useStyles();

    return (
        <div className = {classes.logoGroup} >
            <img className= {classes.logo} src = "images/logo.png" alt = "logo" />
        </div>
    )
}
