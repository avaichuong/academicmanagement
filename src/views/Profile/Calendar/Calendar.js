import React from 'react';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import GroupCard from 'components/GroupCard/GroupCard';

export default function Calendar() {
    return (
        <div>
            <GroupCard
                icon={<i class="material-icons">
                    assessment
                </i>}
                title={"Calendar"}
                content={
                    <div style={{ display: 'flex', justifyContent: 'center' }} >
                        <DayPicker />
                    </div>
                }
            />
        </div>
    )
}
