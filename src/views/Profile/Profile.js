import React, { useEffect, useState } from 'react';
import CardProfile from 'components/CustomerCard/CardProfile';
import Statistics from './Statistics/Statistics';
import Calendar from './Calendar/Calendar';
import NarbarProfile from 'components/CustomerNavBar/NarbarProfile';

export default function Profile() {

    const [user, setUser] = useState();

    useEffect(() => {
        setUser(JSON.parse(localStorage.getItem('user')));
    }, []);

    return (
        <div>
            {/* {console.log(user)} */}
            {/* <NarbarProfile /> */}
            <CardProfile />
            <Statistics />
            <Calendar />
            this is profile
        </div>
    )
}
