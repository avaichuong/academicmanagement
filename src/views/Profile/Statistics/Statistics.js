import React from 'react';
import GroupCard from 'components/GroupCard/GroupCard';
import CardStatistic from 'components/CustomerCard/CardStatistic';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";

export default function Statistics() {
    return (
        <div>
            <GroupCard
                icon={<i class="material-icons">
                    assessment
                </i>}
                title={"Statistics"}
                content={
                    <GridContainer>
                        <GridItem xs={6} sm={6} md={3}>
                            <CardStatistic />
                        </GridItem>
                        <GridItem xs={6} sm={6} md={3}>
                            <CardStatistic />
                        </GridItem>
                        <GridItem xs={6} sm={6} md={3}>
                            <CardStatistic />
                        </GridItem>
                        <GridItem xs={6} sm={6} md={3}>
                            <CardStatistic />
                        </GridItem>
                    </GridContainer>
                }
            />
        </div>
    )
}
