import axios from 'axios';
const qs = require('qs');

const hosts = "http://165.22.56.68/";

export function login(strAPI, data, callback) {
    console.log(data);
    axios({
        method: "POST",
        headers: {
            "content-type": "application/x-www-form-urlencoded",
        },
        data: qs.stringify(data),
        url: hosts + strAPI
    }).then(data => {
        console.log("this is data: ", data);
        callback(data);
    }).catch(function (error) {
        console.log("this is error: ", error);
        callback({
            status: 400,
            statusText: "error",
            msg: "unknow error",
            data: null
        });
    });
}

export function register(strAPI, data, callback) {
    axios({
        method: "POST",
        headers: {
            "content-type": "application/x-www-form-urlencoded",
        },
        data: qs.stringify(data),
        url: hosts + strAPI
    }).then(data => {
        console.log("this is data: ", data);
        callback(data);
    }).catch(function (error) {
        console.log("this is error: ", error);
        callback({
            status: 400,
            statusText: "error",
            msg: "unknow error",
            data: null
        });
    });

}