

//component web

import Home from "views/Home/Home.js";
import Classify from 'views/Classify/Classify.js';
import Profile from 'views/Profile/Profile.js';
import About from 'views/About/About';
import Notification from 'views/Notification/Notification';
import Authentication from 'views/Authentication/Authentication';

const dashboardRoutes = [
  {
    path: "/home",
    name: "Trang chủ",
    icon: "home",
    component: Home,
    layout: "/normal"
  },
  // {
  //   path: "/classify",
  //   name: "Phân loại",
  //   icon: "clear_all",
  //   component: Classify,
  //   layout: "/normal"
  // },
  {
    path: "/profile",
    name: "Profile",
    icon: "home",
    component: Profile,
    layout: "/normal"
  },
  {
    path: "/about",
    name: "About",
    icon: "home",
    component: About,
    layout: "/normal"
  },
  {
    path: "/notification",
    name: "Notification",
    icon: "home",
    component: Notification,
    layout: "/normal"
  },
  {
    path: "/login",
    name: "",
    icon: "",
    component: Authentication,
    layout: "/normal"
  },
];

export default dashboardRoutes;
